﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Helpers;
using Base;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Data;

namespace Service
{
    public class UserService
    {
        public static Boolean authenticate(String Username, String PasswordHash, ref String error)
        {
            String sql = "SELECT COUNT(*) AS jumlah FROM user WHERE username = @username AND password = @password AND status = 1";
            Database DB = Database.query(sql)
                .setDatabaseName(Config.getDatabaseConfig(DatabaseConfigKey.DatabaseName))
                .setParam("@username", Username)
                .setParam("@password", PasswordHash);
            Object result = DB.executeScalar();
            if (result != null && result != DBNull.Value)
            {
                if (Convert.ToInt32(result) != 0)
                    return true;
            }

            if ((result == null || result == DBNull.Value) && DB.MySqlError != "") 
                error = DB.MySqlError;

            return false;
        }

        public static List<String> getAttributes(String Username)
        {
            List<String> Attributes = new List<String>();
            
            if (true)
            {
                String sql = "SELECT nama FROM user WHERE username = @username";
                Database DB = Database.query(sql).setParam("@username", Username);
                DataTable DS = DB.ExecuteQuery();

                if (DS == null && DB.MySqlError != "") throw new Exception(DB.MySqlError);
                else if (DS.Rows.Count > 0)
                {
                    Attributes.Add(DS.Rows[0].ItemArray[0].ToString());
                    Attributes.Add(DS.Rows[0].ItemArray[1].ToString());
                }
            }
            return Attributes;
        }

        public static List<String> getOtoritasUser(String Username, ref String error)
        {
            List<String> otoritas = new List<String>();
            if (true)
            {
                String sql = "SELECT controlid FROM otoritasuser WHERE username LIKE @username";
                Database DB = Database.query(sql).setParam("@username", Username);
                DataTable DS = DB.ExecuteQuery();
                if (DS == null && DB.MySqlError != "") error = DB.MySqlError;
                else if (DS.Rows.Count > 0)
                {
                    foreach (DataRow dr in DS.Rows)
                        otoritas.Add(dr.ItemArray[0].ToString());
                }
            }
            return otoritas;
        }

        public static User getUser(String Username)
        {
            if (true)
            {
                
                String sql = "SELECT username, password, nama, status FROM user WHERE username LIKE @username LIMIT 1";
                Database DB = Database.query(sql).setParam("@username", Username);
                DataTable DS = DB.ExecuteQuery();

                if (DS == null && DB.MySqlError != "") throw new Exception(DB.MySqlError);
                else if (DS.Rows.Count > 0)
                {
                    DataRow DR = DS.Rows[0];
                    User U = new User(DR.ItemArray[0].ToString(), DR.ItemArray[1].ToString(), DR.ItemArray[2].ToString(), DR.ItemArray[3].ToString());
                    return U;
                }
            }
            return null;
        }

        public static List<User> getUserList(ref String error)
        {
            
            if (true)
            {
                String sql = "SELECT username, password, nama, status FROM user";
                Database DB = Database.query(sql);
                DataTable DS = DB.ExecuteQuery();

                if (DS == null && DB.MySqlError != "") error = DB.MySqlError;
                else if (DS.Rows.Count > 0)
                {
                    List<User> users = new List<User>();
                    foreach (DataRow DR in DS.Rows)
                    {
                        User U = new User(DR.ItemArray[0].ToString(), DR.ItemArray[1].ToString(), DR.ItemArray[2].ToString(), DR.ItemArray[3].ToString());
                        users.Add(U);
                    }
                    return users;
                }
            }
            return null;
        }

        public static User getUser(String Username, String PasswordMD5Hash)
        {
            
            String sql = "SELECT username, password, nama, status FROM user WHERE username = @username AND password = @password LIMIT 1";
            Database DB = Database.query(sql);
            DB.setParam("@username", Username);
            DB.setParam("@password", PasswordMD5Hash);
            DataRow DR = DB.ExecuteRow();
            if (DR == null && !String.IsNullOrEmpty(DB.MySqlError))
                throw new Exception(DB.MySqlError);
            else if (DR != null)
            {
                User U = new User(DR.ItemArray[0].ToString(), DR.ItemArray[1].ToString(), DR.ItemArray[2].ToString(), DR.ItemArray[3].ToString());
                return U;
            }
            return null;
        }

        public static User getUser(String username, String passwordMD5Hash, ref String error)
        {
            
            String sql = "SELECT username, password, nama, status FROM user WHERE username = @username AND password = @password LIMIT 1";
            Database DB = Database.query(sql)
                .setParam("@username", username)
                .setParam("@password", passwordMD5Hash);
            DataRow DR = DB.ExecuteRow();
            if (DR == null && !String.IsNullOrEmpty(DB.MySqlError))
                error = DB.MySqlError;
            else if (DR != null)
            {
                User U = new User(DR.ItemArray[0].ToString(), DR.ItemArray[1].ToString(), DR.ItemArray[2].ToString(), DR.ItemArray[3].ToString());
                return U;
            }
            return null;
        }

        public static String calculateMD5(String Text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            Byte[] originalBytes = ASCIIEncoding.Default.GetBytes(Text);
            Byte[] encodedBytes = md5.ComputeHash(originalBytes);
            return Regex.Replace(BitConverter.ToString(encodedBytes), "-", "").ToLower();
        }

        public static Boolean changePassword(String Username, String OldPasswordHash, String NewPasswordHash)
        {
            
            String sql = "UPDATE user SET password = @newpassword WHERE username = @username AND password = @oldpassword";
            Database DB = Database.query(sql);
            DB.setParam("@newpassword", NewPasswordHash);
            DB.setParam("@oldpassword", OldPasswordHash);
            DB.setParam("@username", Username);
            if (DB.ExecuteNonQuery() > 0)
            {
                return true;
            }
            else if (!String.IsNullOrEmpty(DB.MySqlError))
                throw new Exception(DB.MySqlError);
            return false;
        }

        public static Boolean create(User u, ref String error)
        {
            
            String sql = "INSERT INTO user (username, password, nama, status) VALUES (@username, @password, @nama, @status)";
            Database DB = Database.query(sql);
            DB.setParam("@username", u.username);
            DB.setParam("@password", u.passwordHash);
            DB.setParam("@nama", u.nama);
            DB.setParam("@status", u.status);
            if (DB.ExecuteNonQuery() > 0)
            {
                return true;
            }
            else if (!String.IsNullOrEmpty(DB.MySqlError))
                error = DB.MySqlError;
            return false;
        }

        public static Boolean update(User u, ref String error, String oldUsername, Boolean updatePassword)
        {
            String sql = "UPDATE user SET username=@username, nama=@nama, status=@status WHERE username=@oldusername";
            if (updatePassword)
                sql = "UPDATE user SET username=@username, password=@password, nama=@nama, status=@status WHERE username=@oldusername";
            Database DB = Database.query(sql);
            DB.setParam("@username", u.username);
            DB.setParam("@password", u.passwordHash);
            DB.setParam("@nama", u.nama);
            DB.setParam("@status", u.status);
            DB.setParam("@oldusername", oldUsername);
            if (DB.ExecuteNonQuery() > 0)
            {
                return true;
            }
            else if (!String.IsNullOrEmpty(DB.MySqlError))
                error = DB.MySqlError;
            return false;
        }

        public static bool delete(User u, ref String error)
        {
            
            String sql = "DELETE FROM user WHERE username = @username";
            Database DB = Database.query(sql).setParam("@username", u.username);
            if (DB.ExecuteNonQuery() > 0)
            {
                return true;
            }
            else if (!String.IsNullOrEmpty(DB.MySqlError))
                error = DB.MySqlError;
            return false;
        }

        public static DataTable getUsers(ref string error)
        {
            string sql = "SELECT username,password FROM USER";
            Database DB = Database.query(sql);
            DataTable dt= DB.ExecuteQuery();
            error = DB.MySqlError;
            return dt;
        }
    }
}
