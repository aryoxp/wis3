using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Helpers;
using Base.Common;

namespace Service.Common
{
    public class OtoritasService
    {
        public static DataTable getList(ref String error)
        {
            Database DB = Database.query("SELECT controlid, keterangan FROM otoritas ORDER BY controlid ASC");
            DataTable DTOtoritas = (DataTable)DB.ExecuteQuery();

            if (DTOtoritas == null && DB.MySqlError != "") error = DB.MySqlError;
            else return DTOtoritas;
            return null;
        }

        public static List<String> getAllControlIds(ref String error)
        {
            List<String> otoritas = new List<String>();
            Database DB = Database.query("SELECT controlid, keterangan FROM otoritas ORDER BY controlid ASC");
            DataTable DSOtoritas = (DataTable)DB.ExecuteQuery();

            if (DSOtoritas == null && DB.MySqlError != "") error = DB.MySqlError;
            else
            {
                foreach (DataRow dr in DSOtoritas.Rows)
                    otoritas.Add(dr.ItemArray[0].ToString());
            }
            return otoritas;
        }

        public static DataTable getList(String username, ref String error)
        {

            String sql = "SELECT o.controlid, keterangan FROM otoritasuser ou LEFT JOIN otoritas o ON o.controlid = ou.controlid WHERE ou.username = @username ORDER BY controlid ASC";
            Database DB = Database.query(sql).setParam("@username", username);
            DataTable DSOtoritas = (DataTable)DB.ExecuteQuery();

            if (DSOtoritas == null && DB.MySqlError != "") error = DB.MySqlError;
            else return DSOtoritas;
            return null;
        }

        public static Boolean saveOtoritas(Otoritas o, ref String error)
        {
            String sql = "INSERT INTO otoritas (controlid, keterangan) VALUES (@controlid, @keterangan)";
            Database DB = Database.query(sql);
            DB.setParam("@controlid", o.controlid);
            DB.setParam("@keterangan", o.keterangan);
            Int32 Result = DB.ExecuteNonQuery();
            if (Result == 0 && DB.MySqlError != "") error = DB.MySqlError;
            else return true;
            return false;
        }

        public static Boolean DeleteOtoritas(String kode, ref String error)
        {
            
                String sql = "DELETE FROM otoritas WHERE controlid = @controlid";
                Database DB = Database.query(sql);
                DB.setParam("@controlid", kode);
                Int32 Result = DB.ExecuteNonQuery();
                
                if (Result == 0 && DB.MySqlError != "") error = DB.MySqlError;
                else return true;
            return false;
        }

        public static Boolean UpdateOtoritas(Otoritas o, String kodeLama, ref String error)
        {
            
            String sql = "UPDATE otoritas SET controlid = @controlid, keterangan = @keterangan WHERE controlid = @kodelama ";
            Database DB = Database.query(sql);
            DB.setParam("@controlid", o.controlid);
            DB.setParam("@keterangan", o.keterangan);
            DB.setParam("@kodelama", kodeLama);

            Int32 Result = DB.ExecuteNonQuery();

            if (Result == 0 && DB.MySqlError != "") error = DB.MySqlError;
            else return true;
            return false;
        }

        public static List<String> getOtoritasList(string username, ref string error)
        {

            String sql = "SELECT o.controlid FROM otoritasuser ou LEFT JOIN otoritas o ON o.controlid = ou.controlid WHERE ou.username = @username ORDER BY controlid ASC";
            Database DB = Database.query(sql).setParam("@username", username);
            DataTable DSOtoritas = (DataTable)DB.ExecuteQuery();

            if (DSOtoritas == null && DB.MySqlError != "") error = DB.MySqlError;
            else
            {
                List<String> otoritas = new List<string>();
                foreach (DataRow dr in DSOtoritas.Rows)
                {
                    otoritas.Add((String)dr[0]);
                }
                return otoritas;
            } 
            return null;
        }

        public static Boolean authorize(string username, string controlid, bool authorize, ref String error)
        {
            String sql = "INSERT IGNORE INTO otoritasuser (username, controlid) VALUES (@username, @controlid)";
            if (!authorize)
                sql = "DELETE FROM otoritasuser WHERE username = @username AND controlid = @controlid";
            Database DB = Database.query(sql);
            DB.setParam("@username", username);
            DB.setParam("@controlid", controlid);
            Int32 Result = DB.ExecuteNonQuery();
            if (Result == 0 && DB.MySqlError != "") error = DB.MySqlError;
            else return true;
            return false;
        }
    }
}

