﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helpers
{
    public enum DatabaseConfigKey
    {
        Host,
        Port,
        Username,
        Password,
        DatabaseName
    }
}
