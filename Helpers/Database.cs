﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data;

namespace Helpers
{
    public class Database : IDisposable
    {

        private String _connectionString
        {
            get
            {
                return "Server=" + this._ip
                    + ";Database=" + this._db
                    + ";Uid=" + this._username
                    + ";Pwd=" + this._password
                    + ";Port=" + this._port
                    + ";Connection Timeout=" + this._connectionTimeout
                    + ";default command timeout=" + this._commandTimeout;
            }
        }
        
        private String _db;
        private String _ip;
        private String _username;
        private String _password;
        private String _port = "3306";
        private String _connectionTimeout = "5";
        private String _commandTimeout = "30";

        private String _sql = "";
        private List<MySqlParameter> _parameters = new List<MySqlParameter>();

        private MySqlConnection connection;
        private MySqlCommand command;
        private MySqlTransaction transaction;

        private String error = "";


        public Int32 affectedRows = 0;
        public Int64 lastInsertId = 0;
        

        public String MySqlError
        {
            get { return this.error; }
        }
        
        public String sql
        {
            get { return this._sql; }
            set { 
                this._sql = value;
            }
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public Database()
        {
            DatabaseConfig config = Config.readDatabaseConfig();
            this._ip = config.host;
            this._port = config.port;
            this._username = config.username;
            this._password = config.password;
            this._db = config.dbname;
        }

        public Database(String DatabaseName)
            : base()
        {
            this._db = DatabaseName;
        }

        public Database(String DatabaseName, String Username, String Password)
            : base()
        {
            this._db = DatabaseName;
            this._username = Username;
            this._password = Password;
        }

        public Database(String Host, String DatabaseName, String Username, String Password)
            : base()
        {
            this._ip = Host;
            this._db = DatabaseName;
            this._username = Username;
            this._password = Password;
        }

        public Database setParam(String Parameter, Object Value)
        {
            try
            {
                this.error = null;
                this._parameters.Add(new MySqlParameter(Parameter, Value));
            }
            catch { }
            return this;
        }


        public Int32 ExecuteNonQuery()
        {
            this.error = null;

            try
            {
                this.connection = new MySqlConnection(this._connectionString);
                this.command = new MySqlCommand(this._sql, this.connection);
                this.setCommandParameters();
                if (this.command.Connection.State != ConnectionState.Open)
                    this.command.Connection.Open();
                this.affectedRows = this.command.ExecuteNonQuery();
                this.lastInsertId = this.command.LastInsertedId;
                return this.affectedRows;
            }
            catch (Exception e)
            {
                this.error = e.Message;
                return 0;
            }
        }

        public DataTable ExecuteQuery()
        {
            this.error = null;

            try
            {
                this.connection = new MySqlConnection(this._connectionString);
                this.command = new MySqlCommand(this._sql, this.connection);
                this.setCommandParameters();
                if (this.command.Connection.State != ConnectionState.Open)
                    this.command.Connection.Open();
                MySqlDataAdapter DataAdapter = new MySqlDataAdapter(this.command);
                DataAdapter.SelectCommand.Connection = this.command.Connection;
                DataTable Table = new DataTable();
                DataAdapter.Fill(Table);
                return Table;
            }
            catch (Exception e)
            {
                this.error = e.Message;
                return null;
            }
        }

        private void setCommandParameters()
        {
            foreach (MySqlParameter param in _parameters)
            {
                this.command.Parameters.Add(param);
            }
        }

        public Object executeScalar()
        {
            this.error = null;
            try
            {
                this.connection = new MySqlConnection(this._connectionString);
                this.command = new MySqlCommand(this._sql, this.connection);
                this.setCommandParameters();
                if (this.command.Connection.State != ConnectionState.Open)
                    this.command.Connection.Open();
                return this.command.ExecuteScalar();
            }
            catch (Exception e)
            {
                this.error = e.Message;
                return null;
            }
        }

        public DataRow ExecuteRow()
        {
            this.error = null;
            try
            {
                this.connection = new MySqlConnection(this._connectionString);
                this.command = new MySqlCommand(this._sql, this.connection);
                this.setCommandParameters();
                if (this.command.Connection.State != ConnectionState.Open)
                    this.command.Connection.Open();
                MySqlDataAdapter dataAdapter = new MySqlDataAdapter(this.command);
                dataAdapter.SelectCommand.Connection = this.command.Connection;
                DataTable Table = new DataTable();
                dataAdapter.Fill(Table);
                if (Table.Rows.Count > 0)
                    return Table.Rows[0];
            }
            catch (MySqlException e)
            {
                this.error = e.Message;
            }
            return null;
        }

        public void Dispose()
        {
            try
            {
                this.connection.Close();
                this.connection.Dispose();
                this.command.Dispose();
                this.transaction.Dispose();
            }
            catch { }
        }

        public Database BeginTransaction()
        {
            this.error = null;
            try
            {
                this.connection = new MySqlConnection(this._connectionString);
                this.command = new MySqlCommand(this._sql, this.connection);
                if (this.connection.State != ConnectionState.Open)
                    this.connection.Open();
                this.transaction = this.connection.BeginTransaction();
                this.command.Transaction = this.transaction;
            }
            catch { }
            return this;
        }

        public void Commit() { this.transaction.Commit(); }
        public void Rollback() { this.transaction.Rollback(); }
        public void Close() { this.connection.Close(); }

        /// Query chaining helper methods

        public static Database query(String sql)
        {
            Database db = new Database();
            db._sql = sql;
            return db;
        }

        public Database setDatabaseName(String dbName)
        {
            this._db = dbName;
            return this;
        }

        public Database withParameter(String Parameter, Object Value)
        {
            return this.setParam(Parameter, Value);
        }

        public static Boolean connectionTest()
        {
            try
            {
                Object result = Database.query("SELECT true").executeScalar();
                String res = result.ToString();
                return (res.Equals("1"));
            }
            catch (Exception ex){
                ex.ToString();
                return false; 
            }
        }
    }
}
