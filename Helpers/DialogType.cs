﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helpers
{
    public enum DialogType
    {
        DialogInfo,
        DialogSuccess,
        DialogWarning,
        DialogError
    }
}
