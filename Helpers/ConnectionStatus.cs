﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WIS3.Base
{
    public enum ConnectionStatus
    {
        NotConnected,
        Connecting,
        Connected
    }
}
