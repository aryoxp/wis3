﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.IO;
using Newtonsoft.Json.Linq;

namespace Helpers
{
    public class Config
    {
        public static DatabaseConfig readDatabaseConfig()
        {
            try {
                String databaseJson = File.ReadAllText("db.json");
                DatabaseConfig config = JsonConvert.DeserializeObject<DatabaseConfig>(databaseJson);
                return config;
            } catch (Exception ex) {
                throw ex;
            }
        }

        public static Boolean saveDatabaseConfig(DatabaseConfig config)
        {
            try
            {
                String databaseJson = JsonConvert.SerializeObject(config);
                File.WriteAllText("db.json", databaseJson);
                return true;
            }
            catch { return false; }
        }

        public static String getDatabaseConfig(DatabaseConfigKey key)
        {
            try
            {
                DatabaseConfig config = Config.readDatabaseConfig();
                switch (key)
                {
                    case DatabaseConfigKey.Host:
                        return config.host;
                    case DatabaseConfigKey.Port:
                        return config.port;
                    case DatabaseConfigKey.Username:
                        return config.username;
                    case DatabaseConfigKey.Password:
                        return config.password;
                    case DatabaseConfigKey.DatabaseName:
                        return config.dbname;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        public static String read(String key)
        {
            try
            {
                String configJson = File.ReadAllText("config.json");
                JObject configObject = JObject.Parse(configJson);
                JToken configToken = configObject[key];
                return configToken.Value<String>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
