﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Helpers
{
    public partial class PDFViewer : Form
    {
        private String filename;
        private PDFViewType type = PDFViewType.Normal;

        public PDFViewer()
        {
            InitializeComponent();
        }

        public PDFViewer(String filename) : this()
        {
            this.filename = filename;
            axAcrobat.Size = new Size(this.ClientSize.Width, this.ClientSize.Height - this.btPrint.Height - 30);
            axAcrobat.Location = new Point(0, 0);
            axAcrobat.LoadFile(filename);
        }

        public PDFViewer(string filename, PDFViewType type) : this(filename)
        {
            this.type = type;
            switch (type)
            {
                case PDFViewType.FullScreen:
                    this.WindowState = FormWindowState.Maximized;
                    break;
                case PDFViewType.Large:
                    Rectangle workingRectangle = Screen.PrimaryScreen.WorkingArea;
                    this.Size = new System.Drawing.Size(
                        workingRectangle.Width - (int)(0.1 * workingRectangle.Width),
                        workingRectangle.Height - (int)(0.1 * workingRectangle.Height));
                    this.Location = new System.Drawing.Point((int)(workingRectangle.Width*0.05), (int)(workingRectangle.Height * 0.05));
                    break;
                default:
                    break;
            }
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btPrint_Click(object sender, EventArgs e)
        {
            this.axAcrobat.printWithDialog();
        }

        public static void showPDF(String filename) 
        {
            new PDFViewer(filename).ShowDialog();
        }

        public static void showPDF(String filename, PDFViewType type)
        {
            new PDFViewer(filename, type).ShowDialog();
        }
    }
}
