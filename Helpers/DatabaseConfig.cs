﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helpers
{
    public class DatabaseConfig
    {
        public String username { get; set; }
        public String password { get; set; }
        public String host { get; set; }
        public String port { get; set; }
        public String dbname { get; set; }
    }
}
