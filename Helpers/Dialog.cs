﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Helpers
{
    public partial class Dialog : Form
    {

        public DialogType type = DialogType.DialogInfo;

        public Dialog()
            : this("")
        {}

        public Dialog(String message)
            : this(message, DialogType.DialogInfo)
        {}

        public Dialog(String Message, DialogType type)
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterParent;
            this.tMessage.Text = Message;
            this.type = type;

            this.MinimumSize = new Size(this.tMessage.Width + 50, this.tMessage.Height + 10 + this.btOK.Height + 10);
            this.btOK.Location = new Point((this.ClientSize.Width/2) - btOK.Width/2, this.tMessage.Location.Y + this.tMessage.Height + 20);
            if (this.ClientSize.Width > 310)
                this.btOK.Location = new Point((this.ClientSize.Width / 2) - btOK.Width / 2 + icon.Width / 2, this.tMessage.Location.Y + this.tMessage.Height + 20);
            this.ClientSize = new Size(this.ClientSize.Width + 50, this.btOK.Location.Y + this.btOK.Height + 20);
            this.AutoSize = true;
            this.AutoSizeMode = AutoSizeMode.GrowOnly;
            this.icon.Location = new Point(this.icon.Location.X, this.tMessage.Top-(64-tMessage.Height)/2);

            switch (this.type)
            {
                case DialogType.DialogError:
                    this.icon.Image = Properties.Resources.ic_cancel;
                    this.Text = "Error";
                    break;
                case DialogType.DialogSuccess:
                    this.icon.Image = Properties.Resources.ic_checked;
                    this.Text = "Success";
                    break;
                case DialogType.DialogWarning:
                    this.Text = "Warning";
                    break;
                case DialogType.DialogInfo:
                default:
                    Image i = this.icon.Image;
                    i.RotateFlip(RotateFlipType.Rotate180FlipNone);
                    break;
            }
        }

        public static Dialog newDialog(String message, DialogType type)
        {
            return new Dialog(message, type);
        }

        public static Dialog newDialog(String message)
        {
            return new Dialog(message);
        }

        public static void showError(String error)
        {
            Dialog.newDialog(error, DialogType.DialogError).ShowDialog();
        }

        public static void showWarning(String warning)
        {
            Dialog.newDialog(warning, DialogType.DialogWarning).ShowDialog();
        }

        public static void showSuccess(String message)
        {
            Dialog.newDialog(message, DialogType.DialogSuccess).ShowDialog();
        }

        public static void showInformation(String info)
        {
            Dialog.newDialog(info, DialogType.DialogInfo).ShowDialog();
        }

        public static void show(String message)
        {
            Dialog.newDialog(message).ShowDialog();
        }

        private void btOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }
    }
}
