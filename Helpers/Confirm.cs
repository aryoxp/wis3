﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Helpers
{
    public partial class Confirm : Form
    {

        public Confirm()
            : this("")
        {}

        public Confirm(String message) {

            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterParent;
            this.tMessage.Text = message;
            
            this.MinimumSize = new Size(this.tMessage.Width + 50, this.tMessage.Height + 10 + this.btYes.Height + 10);
            this.ClientSize = new Size(this.ClientSize.Width, this.btYes.Location.Y + this.btYes.Height + 20);
            this.AutoSize = true;
            this.AutoSizeMode = AutoSizeMode.GrowOnly;
            this.icon.Location = new Point(this.icon.Location.X, this.tMessage.Top-(64-tMessage.Height)/2);

        }

        public static Confirm newDialog(String message)
        {
            return new Confirm(message);
        }

        private void btYes_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Yes;
        }

        private void btNo_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
        }

        public static Boolean confirm(String text)
        {
            Confirm confirm = Confirm.newDialog(text);
            if (confirm.ShowDialog() == DialogResult.Yes)
                return true;
            else return false;
        }

    }
}
