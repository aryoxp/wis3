﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WIS3.Interfaces;
using Helpers;
using Service;

namespace Controllers.Akademik
{
    public partial class UserController : Controller
    {
        public UserController():base()
        {
            InitializeComponent();
        }
        public UserController(IMainInterface maininterface)
            : base(maininterface)
        {
            this.mainInterface = maininterface;
            InitializeComponent();
        }

        private void btLoad_Click(object sender, EventArgs e)
        {
            bwLoadUser.RunWorkerAsync();
        }

        private void bwLoadUser_DoWork(object sender, DoWorkEventArgs e)
        {
            string err=null;
            DataTable dt = UserService.getUsers(ref err);
            e.Result = new Object[] { dt, err };
        }

        private void bwLoadUser_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Object[] result = (Object[])e.Result;
            DataTable dt = (DataTable) result[0];
            String err = (String)result[1];
            if (err != null)
                Dialog.showError(err);
            dgUsers.DataSource = dt;
            Dialog.showInformation("ok");
        }
    }
}
