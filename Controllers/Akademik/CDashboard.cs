﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Base;
using WIS3.Interfaces;
using Helpers;

namespace Controllers.Akademik
{
    public partial class CDashboard : Controller
    {

        public CDashboard() : base()
        {
            InitializeComponent();
        }

        public CDashboard(IMainInterface mainInterface)
            : base(mainInterface)
        {
            InitializeComponent();
        }

        private void btOK_Click(object sender, EventArgs e)
        {
            this.mainInterface.loadController("Controllers.Common.Login");
        }
    }
}
