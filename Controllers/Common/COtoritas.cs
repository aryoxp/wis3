using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Base;
using WIS3.Interfaces;
using Controllers.Common;
using Base.Common;
using Helpers;
using Controllers;
using Service.Common;


namespace Controllers.Common
{
    public partial class COtoritas : Controller
    {

        public COtoritas(): base()
        {
            InitializeComponent();
        }


        public COtoritas(IMainInterface mainInterface)
            : base(mainInterface)
        {
            InitializeComponent();
            if (!this.ListWorker.IsBusy)
                this.ListWorker.RunWorkerAsync();
            this.mainInterface.setStatus("Loading authorities...");
        }

        private void ListWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            String error = null;
            DataTable DTOtoritas = OtoritasService.getList(ref error);
            e.Result = new Object[] { DTOtoritas, error };
        }

        private void ListWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                Object[] result = (Object[])e.Result;
                DataTable dtOtoritas = (DataTable)result[0];
                String error = (String)result[1];
                this.dgOtoritas.DataSource = dtOtoritas;
                this.dgOtoritas.Columns[0].HeaderText = "Kode Otoritas";
                this.dgOtoritas.Columns[1].HeaderText = "Keterangan";
                if (error != null)
                    Dialog.showError(error);
            }
            catch
            {
                this.dgOtoritas.DataSource = null;
            }
            this.mainInterface.setStatusReady();
        }

        private void btBaru_Click(object sender, EventArgs e)
        {
            FOtoritas f = new FOtoritas();
            DialogResult dr = f.ShowDialog();
           
            if(dr == DialogResult.OK && f.otoritas != null)
            {
                Otoritas o = f.otoritas;
                if (!SaveWorker.IsBusy)
                {
                    this.SaveWorker.RunWorkerAsync(o);
                    this.mainInterface.setStatus("Saving otoritas");
                    this.temporary = o;
                }
            }
        }

        private Otoritas temporary;

        private void SaveWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            String error = "";
            Boolean result = OtoritasService.saveOtoritas((Otoritas)e.Argument, ref error);
            e.Result = new Object[] { result, error };
        }

        private void SaveWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Object[] result = (Object[])e.Result;
            Boolean success = (Boolean)result[0];
            String error = (String)result[1];
            if (success)
            {
                if (!this.ListWorker.IsBusy)
                    this.ListWorker.RunWorkerAsync();
                this.mainInterface.setStatus("Loading authorities...");
                Dialog.showSuccess("Data otoritas telah disimpan");
                this.temporary = null;
            }
            else
            {
                Dialog.showError(error);
                if (this.temporary != null)
                {
                    FOtoritas f = new FOtoritas();
                    f.setOtoritas(this.temporary);
                    DialogResult dr = f.ShowDialog();

                    if (dr == DialogResult.OK && f.otoritas != null)
                    {
                        Otoritas o = f.otoritas;
                        if (!SaveWorker.IsBusy)
                        {
                            this.SaveWorker.RunWorkerAsync(o);
                            this.mainInterface.setStatus("Saving otoritas");
                            this.temporary = o;
                        }
                    }
                }
            }
            this.mainInterface.setStatusReady();
        }

        private void btDelete_Click(object sender, EventArgs e)
        {
            if (this.dgOtoritas != null && this.dgOtoritas.SelectedRows.Count == 1)
            {
                if (Confirm.confirm("Hapus otoritas yang dipilih?"))
                {
                    String controlid = this.dgOtoritas.SelectedRows[0].Cells[0].Value.ToString();
                    if (!this.DeleteWorker.IsBusy)
                        this.DeleteWorker.RunWorkerAsync(controlid);
                    this.mainInterface.setStatus("Deleting...");
                }
            }
            else Dialog.showInformation("Silakan pilih otoritas yang akan dihapus.");
        }

        private void DeleteWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            String controlid = (String)e.Argument;
            String error = "";
            Boolean result = OtoritasService.DeleteOtoritas(controlid, ref error);
            e.Result = new Object[] { result, error };
        }

        private void DeleteWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Object[] result = (Object[])e.Result;
            Boolean success = (Boolean)result[0];
            String error = (String)result[1];
            if (success)
            {
                if (!this.ListWorker.IsBusy)
                    this.ListWorker.RunWorkerAsync();
                this.mainInterface.setStatus("Loading authorities...");
                Dialog.showSuccess("Data otoritas telah dihapus");
            }
            else
            {
                Dialog.showError(error);
            }
            this.mainInterface.setStatusReady();
        }

        private void btEdit_Click(object sender, EventArgs e)
        {
            if (this.dgOtoritas != null && this.dgOtoritas.SelectedRows.Count == 1)
            {
                String kode = this.dgOtoritas.SelectedRows[0].Cells[0].Value.ToString();
                String keterangan = this.dgOtoritas.SelectedRows[0].Cells[1].Value.ToString();
                Otoritas o = new Otoritas(kode, keterangan);
                FOtoritas f = new FOtoritas(this.mainInterface, o);
                if (f.ShowDialog() == DialogResult.OK)
                {
                    if (!this.UpdateWorker.IsBusy)
                        this.UpdateWorker.RunWorkerAsync(new Object[] { f.otoritas, f.kodeLama });
                    this.mainInterface.setStatus("Updating...");
                }
            }
            else Dialog.showInformation("Silakan pilih otoritas yang akan diedit.");
        }

        private void UpdateWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            Object[] args = (Object[])e.Argument;
            Otoritas o = (Otoritas)args[0];
            String kodeLama = (String)args[1];
            String error = "";
            Boolean result = OtoritasService.UpdateOtoritas(o, kodeLama, ref error);
            e.Result = new Object[] { result, error };
        }

        private void UpdateWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Object[] result = (Object[])e.Result;
            Boolean success = (Boolean)result[0];
            String error = (String)result[1];
            if (success)
            {
                if (!this.ListWorker.IsBusy)
                    this.ListWorker.RunWorkerAsync();
                this.mainInterface.setStatus("Loading authorities...");
                Dialog.showSuccess("Data otoritas telah diupdate");
                this.temporary = null;
            }
            else
            {
                Dialog.showError(error);
            }
            this.mainInterface.setStatusReady();
        }

        private void btRefresh_Click(object sender, EventArgs e)
        {
            if (!this.ListWorker.IsBusy)
                this.ListWorker.RunWorkerAsync();
            this.mainInterface.setStatus("Loading authorities...");
            this.temporary = null;
        }
    }
}