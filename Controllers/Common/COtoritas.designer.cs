namespace Controllers.Common
{
    partial class COtoritas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.GroupBox gbPegawai;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.UpdateWorker = new System.ComponentModel.BackgroundWorker();
            this.SaveWorker = new System.ComponentModel.BackgroundWorker();
            this.DeleteWorker = new System.ComponentModel.BackgroundWorker();
            this.ListWorker = new System.ComponentModel.BackgroundWorker();
            this.btPrint = new System.Windows.Forms.Button();
            this.btRefresh = new System.Windows.Forms.Button();
            this.btBaru = new System.Windows.Forms.Button();
            this.btDelete = new System.Windows.Forms.Button();
            this.btEdit = new System.Windows.Forms.Button();
            this.dgOtoritas = new System.Windows.Forms.DataGridView();
            gbPegawai = new System.Windows.Forms.GroupBox();
            gbPegawai.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgOtoritas)).BeginInit();
            this.SuspendLayout();
            // 
            // UpdateWorker
            // 
            this.UpdateWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.UpdateWorker_DoWork);
            this.UpdateWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.UpdateWorker_RunWorkerCompleted);
            // 
            // SaveWorker
            // 
            this.SaveWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.SaveWorker_DoWork);
            this.SaveWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.SaveWorker_RunWorkerCompleted);
            // 
            // DeleteWorker
            // 
            this.DeleteWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.DeleteWorker_DoWork);
            this.DeleteWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.DeleteWorker_RunWorkerCompleted);
            // 
            // ListWorker
            // 
            this.ListWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.ListWorker_DoWork);
            this.ListWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.ListWorker_RunWorkerCompleted);
            // 
            // gbPegawai
            // 
            gbPegawai.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            gbPegawai.Controls.Add(this.btPrint);
            gbPegawai.Controls.Add(this.btRefresh);
            gbPegawai.Controls.Add(this.btBaru);
            gbPegawai.Controls.Add(this.btDelete);
            gbPegawai.Controls.Add(this.btEdit);
            gbPegawai.Controls.Add(this.dgOtoritas);
            gbPegawai.Location = new System.Drawing.Point(14, 15);
            gbPegawai.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            gbPegawai.Name = "gbPegawai";
            gbPegawai.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            gbPegawai.Size = new System.Drawing.Size(724, 431);
            gbPegawai.TabIndex = 1;
            gbPegawai.TabStop = false;
            gbPegawai.Text = "Daftar Otoritas";
            // 
            // btPrint
            // 
            this.btPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btPrint.Enabled = false;
            this.btPrint.Image = global::Controllers.Properties.Resources.printer;
            this.btPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btPrint.Location = new System.Drawing.Point(630, 385);
            this.btPrint.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btPrint.Name = "btPrint";
            this.btPrint.Padding = new System.Windows.Forms.Padding(12, 0, 12, 0);
            this.btPrint.Size = new System.Drawing.Size(87, 38);
            this.btPrint.TabIndex = 21;
            this.btPrint.Text = "Print";
            this.btPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btPrint.UseVisualStyleBackColor = true;
            // 
            // btRefresh
            // 
            this.btRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btRefresh.Image = global::Controllers.Properties.Resources.arrow_refresh;
            this.btRefresh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btRefresh.Location = new System.Drawing.Point(252, 385);
            this.btRefresh.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btRefresh.Name = "btRefresh";
            this.btRefresh.Padding = new System.Windows.Forms.Padding(6, 0, 0, 0);
            this.btRefresh.Size = new System.Drawing.Size(87, 38);
            this.btRefresh.TabIndex = 18;
            this.btRefresh.Text = "Refresh";
            this.btRefresh.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btRefresh.UseVisualStyleBackColor = true;
            this.btRefresh.Click += new System.EventHandler(this.btRefresh_Click);
            // 
            // btBaru
            // 
            this.btBaru.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btBaru.Image = global::Controllers.Properties.Resources.table_add;
            this.btBaru.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btBaru.Location = new System.Drawing.Point(347, 385);
            this.btBaru.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btBaru.Name = "btBaru";
            this.btBaru.Padding = new System.Windows.Forms.Padding(12, 0, 12, 0);
            this.btBaru.Size = new System.Drawing.Size(87, 38);
            this.btBaru.TabIndex = 17;
            this.btBaru.Text = "Baru";
            this.btBaru.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btBaru.UseVisualStyleBackColor = true;
            this.btBaru.Click += new System.EventHandler(this.btBaru_Click);
            // 
            // btDelete
            // 
            this.btDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btDelete.Image = global::Controllers.Properties.Resources.table_delete;
            this.btDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btDelete.Location = new System.Drawing.Point(536, 385);
            this.btDelete.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btDelete.Name = "btDelete";
            this.btDelete.Padding = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.btDelete.Size = new System.Drawing.Size(87, 38);
            this.btDelete.TabIndex = 16;
            this.btDelete.Text = "Delete";
            this.btDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btDelete.UseVisualStyleBackColor = true;
            this.btDelete.Click += new System.EventHandler(this.btDelete_Click);
            // 
            // btEdit
            // 
            this.btEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btEdit.Image = global::Controllers.Properties.Resources.table_edit;
            this.btEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btEdit.Location = new System.Drawing.Point(441, 385);
            this.btEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btEdit.Name = "btEdit";
            this.btEdit.Padding = new System.Windows.Forms.Padding(12, 0, 12, 0);
            this.btEdit.Size = new System.Drawing.Size(87, 38);
            this.btEdit.TabIndex = 15;
            this.btEdit.Text = "Edit";
            this.btEdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btEdit.UseVisualStyleBackColor = true;
            this.btEdit.Click += new System.EventHandler(this.btEdit_Click);
            // 
            // dgOtoritas
            // 
            this.dgOtoritas.AllowUserToAddRows = false;
            this.dgOtoritas.AllowUserToDeleteRows = false;
            this.dgOtoritas.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgOtoritas.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgOtoritas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgOtoritas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgOtoritas.BackgroundColor = System.Drawing.Color.White;
            this.dgOtoritas.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgOtoritas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgOtoritas.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgOtoritas.Location = new System.Drawing.Point(7, 25);
            this.dgOtoritas.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dgOtoritas.MultiSelect = false;
            this.dgOtoritas.Name = "dgOtoritas";
            this.dgOtoritas.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgOtoritas.RowHeadersVisible = false;
            this.dgOtoritas.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgOtoritas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgOtoritas.Size = new System.Drawing.Size(710, 352);
            this.dgOtoritas.TabIndex = 14;
            // 
            // VOtoritas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(gbPegawai);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "VOtoritas";
            this.Size = new System.Drawing.Size(752, 460);
            gbPegawai.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgOtoritas)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.ComponentModel.BackgroundWorker UpdateWorker;
        private System.ComponentModel.BackgroundWorker SaveWorker;
        private System.ComponentModel.BackgroundWorker DeleteWorker;
        private System.ComponentModel.BackgroundWorker ListWorker;
        private System.Windows.Forms.Button btPrint;
        private System.Windows.Forms.Button btRefresh;
        private System.Windows.Forms.Button btBaru;
        private System.Windows.Forms.Button btDelete;
        private System.Windows.Forms.Button btEdit;
        private System.Windows.Forms.DataGridView dgOtoritas;
    }
}