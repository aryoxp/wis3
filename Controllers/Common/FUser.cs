using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using Base;
using WIS3.Interfaces;
using Helpers;

namespace Controllers.Common
{
    public partial class FUser : Form
    {
        private String username {
            get { return this.tbUsername.Text.Trim(); }
            set { this.tbUsername.Text = value.Trim(); }
        }
        private String Password {
            get { return this.tbPassword.Text.Trim(); }
            set { this.tbPassword.Text = value.Trim(); }
        }

        public User User { get; private set; }
        public String oldUsername;

        private IMainInterface iMainForm;
        public User newUser;

        public FUser(IMainInterface iMainForm) : this()
        {
            this.iMainForm = iMainForm;
        }

        public FUser()
        {
            this.InitializeComponent();
        }

        public FUser(User u): this()
        {
            this.tbUsername.Text = u.username;
            this.tbNama.Text = u.nama;
            this.oldUsername = u.username;
            if (u.status.Equals("True") || u.status.Equals("1"))
                this.cbEnabled.Checked = true;
        }

        public static DialogResult showDialog(IMainInterface iMainForm)
        {
            FUser f = new FUser(iMainForm);
            return f.ShowDialog();
        }

        private static String MD5(String Text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            Byte[] originalBytes = ASCIIEncoding.Default.GetBytes(Text);
            Byte[] encodedBytes = md5.ComputeHash(originalBytes);
            return Regex.Replace(BitConverter.ToString(encodedBytes), "-", "").ToLower();
        }

        private void btOK_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(this.tbNama.Text.Trim()))
            {
                Dialog.showWarning("nama tidak boleh kosong.");
                return;
            }
            if (String.IsNullOrEmpty(this.tbUsername.Text.Trim()))
            {
                Dialog.showWarning("username tidak boleh kosong.");
                return;
            }
            if (this.oldUsername == null && String.IsNullOrEmpty(this.tbPassword.Text.Trim()))
            {
                Dialog.showWarning("Password tidak boleh kosong.");
                return;
            }

            this.newUser = new User(this.tbUsername.Text.Trim(), MD5(this.tbPassword.Text.Trim()), this.tbNama.Text.Trim(), this.cbEnabled.Checked);
            this.DialogResult = DialogResult.OK;

        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            this.newUser = null;
            this.DialogResult = DialogResult.Cancel;
        }

        public String getPasswordText()
        {
            return this.tbPassword.Text.Trim();
        }
    }
}