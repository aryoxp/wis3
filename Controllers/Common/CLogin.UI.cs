﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Controllers.Common
{
    public partial class CLogin
    {
        private void init()
        {
            tbUsername.Text = "Username";
            tbUsername.ForeColor = Color.LightGray;
            tbUsername.Font = new Font(tbUsername.Font, FontStyle.Italic);
            tbPassword.PasswordChar = '\0';
            tbPassword.ForeColor = Color.LightGray;
            tbPassword.Font = new Font(tbPassword.Font, FontStyle.Italic);
            this.mainInterface.setStatusReady();


            cheat(); // shortcut

        }

        private void cheat()
        {
            this.tbPassword.Text = "rahasia";
            this.tbUsername.Text = "admin";
        }

        private void Login_Resize(object sender, EventArgs e)
        {
            loginGroupBox.Location = new Point(this.Size.Width / 2 - loginGroupBox.Width / 2, this.Size.Height / 2 - loginGroupBox.Height / 2);
        }

        private void tbUsername_Enter(object sender, EventArgs e)
        {
            if (tbUsername.Text.Trim().Equals("Username"))
            {
                this.tbUsername.Text = "";
                this.tbUsername.ForeColor = Color.Black;
                this.tbUsername.Font = new Font(tbUsername.Font, FontStyle.Regular);
            }
        }
        private void tbUsername_Leave(object sender, EventArgs e)
        {
            if (this.tbUsername.Text.Trim().Equals(""))
            {
                this.tbUsername.Text = "Username";
                this.tbUsername.ForeColor = Color.LightGray;
                this.tbUsername.Font = new Font(tbUsername.Font, FontStyle.Italic);
            }
        }
        private void tbPassword_Enter(object sender, EventArgs e)
        {
            if (this.tbPassword.Text.Trim().Equals("Password"))
            {
                this.tbPassword.Text = "";
                this.tbPassword.ForeColor = Color.Black;
                this.tbPassword.Font = new Font(tbUsername.Font, FontStyle.Regular);
                this.tbPassword.PasswordChar = '*';
            }
        }
        private void tbPassword_Leave(object sender, EventArgs e)
        {
            if (this.tbPassword.Text.Trim().Equals(""))
            {
                this.tbPassword.Text = "Password";
                this.tbPassword.ForeColor = Color.LightGray;
                this.tbPassword.Font = new Font(tbUsername.Font, FontStyle.Italic);
                this.tbPassword.PasswordChar = '\0';
            }
        }
    }
}
