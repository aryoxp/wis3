namespace Controllers.Common
{
    partial class COtoritasUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.UpdateWorker = new System.ComponentModel.BackgroundWorker();
            this.SaveWorker = new System.ComponentModel.BackgroundWorker();
            this.DeleteWorker = new System.ComponentModel.BackgroundWorker();
            this.ListWorker = new System.ComponentModel.BackgroundWorker();
            this.gbPegawai = new System.Windows.Forms.GroupBox();
            this.btPrint = new System.Windows.Forms.Button();
            this.btOtoritas = new System.Windows.Forms.Button();
            this.dgOtoritas = new System.Windows.Forms.DataGridView();
            this.btRefresh = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgUsers = new System.Windows.Forms.DataGridView();
            this.listUserWorker = new System.ComponentModel.BackgroundWorker();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.bwOtoritasUser = new System.Windows.Forms.Button();
            this.gbPegawai.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgOtoritas)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgUsers)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // SaveWorker
            // 
            this.SaveWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.SaveWorker_DoWork);
            this.SaveWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.SaveWorker_RunWorkerCompleted);
            // 
            // ListWorker
            // 
            this.ListWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.ListWorker_DoWork);
            this.ListWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.ListWorker_RunWorkerCompleted);
            // 
            // gbPegawai
            // 
            this.gbPegawai.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbPegawai.Controls.Add(this.bwOtoritasUser);
            this.gbPegawai.Controls.Add(this.btPrint);
            this.gbPegawai.Controls.Add(this.btOtoritas);
            this.gbPegawai.Controls.Add(this.dgOtoritas);
            this.gbPegawai.Location = new System.Drawing.Point(3, 4);
            this.gbPegawai.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gbPegawai.Name = "gbPegawai";
            this.gbPegawai.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gbPegawai.Size = new System.Drawing.Size(608, 452);
            this.gbPegawai.TabIndex = 1;
            this.gbPegawai.TabStop = false;
            this.gbPegawai.Text = "Daftar Otoritas";
            // 
            // btPrint
            // 
            this.btPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btPrint.Enabled = false;
            this.btPrint.Image = global::Controllers.Properties.Resources.printer;
            this.btPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btPrint.Location = new System.Drawing.Point(514, 406);
            this.btPrint.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btPrint.Name = "btPrint";
            this.btPrint.Padding = new System.Windows.Forms.Padding(12, 0, 12, 0);
            this.btPrint.Size = new System.Drawing.Size(87, 38);
            this.btPrint.TabIndex = 21;
            this.btPrint.Text = "Print";
            this.btPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btPrint.UseVisualStyleBackColor = true;
            // 
            // btOtoritas
            // 
            this.btOtoritas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btOtoritas.Image = global::Controllers.Properties.Resources.table_add;
            this.btOtoritas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btOtoritas.Location = new System.Drawing.Point(7, 406);
            this.btOtoritas.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btOtoritas.Name = "btOtoritas";
            this.btOtoritas.Padding = new System.Windows.Forms.Padding(12, 0, 12, 0);
            this.btOtoritas.Size = new System.Drawing.Size(113, 38);
            this.btOtoritas.TabIndex = 17;
            this.btOtoritas.Text = "Otoritas";
            this.btOtoritas.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btOtoritas.UseVisualStyleBackColor = true;
            this.btOtoritas.Click += new System.EventHandler(this.btOtoritas_Click);
            // 
            // dgOtoritas
            // 
            this.dgOtoritas.AllowUserToAddRows = false;
            this.dgOtoritas.AllowUserToDeleteRows = false;
            this.dgOtoritas.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgOtoritas.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgOtoritas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgOtoritas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgOtoritas.BackgroundColor = System.Drawing.Color.White;
            this.dgOtoritas.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgOtoritas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgOtoritas.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgOtoritas.Location = new System.Drawing.Point(7, 25);
            this.dgOtoritas.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dgOtoritas.MultiSelect = false;
            this.dgOtoritas.Name = "dgOtoritas";
            this.dgOtoritas.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgOtoritas.RowHeadersVisible = false;
            this.dgOtoritas.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgOtoritas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgOtoritas.Size = new System.Drawing.Size(594, 374);
            this.dgOtoritas.TabIndex = 14;
            this.dgOtoritas.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgOtoritas_CellDoubleClick);
            // 
            // btRefresh
            // 
            this.btRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btRefresh.Image = global::Controllers.Properties.Resources.arrow_refresh;
            this.btRefresh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btRefresh.Location = new System.Drawing.Point(209, 406);
            this.btRefresh.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btRefresh.Name = "btRefresh";
            this.btRefresh.Padding = new System.Windows.Forms.Padding(6, 0, 0, 0);
            this.btRefresh.Size = new System.Drawing.Size(87, 38);
            this.btRefresh.TabIndex = 18;
            this.btRefresh.Text = "Refresh";
            this.btRefresh.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btRefresh.UseVisualStyleBackColor = true;
            this.btRefresh.Click += new System.EventHandler(this.btRefresh_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.dgUsers);
            this.groupBox1.Controls.Add(this.btRefresh);
            this.groupBox1.Location = new System.Drawing.Point(3, 4);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Size = new System.Drawing.Size(303, 452);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "User";
            // 
            // dgUsers
            // 
            this.dgUsers.AllowUserToAddRows = false;
            this.dgUsers.AllowUserToDeleteRows = false;
            this.dgUsers.AllowUserToResizeRows = false;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgUsers.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgUsers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgUsers.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgUsers.BackgroundColor = System.Drawing.Color.White;
            this.dgUsers.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgUsers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgUsers.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgUsers.Location = new System.Drawing.Point(7, 25);
            this.dgUsers.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dgUsers.MultiSelect = false;
            this.dgUsers.Name = "dgUsers";
            this.dgUsers.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgUsers.RowHeadersVisible = false;
            this.dgUsers.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgUsers.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgUsers.Size = new System.Drawing.Size(289, 374);
            this.dgUsers.TabIndex = 15;
            this.dgUsers.SelectionChanged += new System.EventHandler(this.dgUsers_SelectionChanged);
            // 
            // listUserWorker
            // 
            this.listUserWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.listUserWorker_DoWork);
            this.listUserWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.listUserWorker_RunWorkerCompleted);
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.gbPegawai);
            this.splitContainer.Size = new System.Drawing.Size(927, 460);
            this.splitContainer.SplitterDistance = 309;
            this.splitContainer.TabIndex = 3;
            // 
            // bwOtoritasUser
            // 
            this.bwOtoritasUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bwOtoritasUser.Image = global::Controllers.Properties.Resources.arrow_refresh;
            this.bwOtoritasUser.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bwOtoritasUser.Location = new System.Drawing.Point(421, 406);
            this.bwOtoritasUser.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.bwOtoritasUser.Name = "bwOtoritasUser";
            this.bwOtoritasUser.Padding = new System.Windows.Forms.Padding(6, 0, 0, 0);
            this.bwOtoritasUser.Size = new System.Drawing.Size(87, 38);
            this.bwOtoritasUser.TabIndex = 19;
            this.bwOtoritasUser.Text = "Refresh";
            this.bwOtoritasUser.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bwOtoritasUser.UseVisualStyleBackColor = true;
            this.bwOtoritasUser.Click += new System.EventHandler(this.bwOtoritasUser_Click);
            // 
            // VOtoritasUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "VOtoritasUser";
            this.Size = new System.Drawing.Size(927, 460);
            this.gbPegawai.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgOtoritas)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgUsers)).EndInit();
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.ComponentModel.BackgroundWorker UpdateWorker;
        private System.ComponentModel.BackgroundWorker SaveWorker;
        private System.ComponentModel.BackgroundWorker DeleteWorker;
        private System.ComponentModel.BackgroundWorker ListWorker;
        private System.Windows.Forms.GroupBox gbPegawai;
        private System.Windows.Forms.Button btPrint;
        private System.Windows.Forms.Button btRefresh;
        private System.Windows.Forms.Button btOtoritas;
        private System.Windows.Forms.DataGridView dgOtoritas;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgUsers;
        private System.ComponentModel.BackgroundWorker listUserWorker;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.Button bwOtoritasUser;
    }
}