﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Base;
using Service;
using Helpers;

namespace Controllers.Common
{
    public partial class FPassword : Form
    {
        private User user;

        public FPassword(User U)
        {
            InitializeComponent();
            this.user = U;
            this.tbUsername.Text = U.username;
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btOK_Click(object sender, EventArgs e)
        {
            Boolean continueChange = true;
            String OldPasswordHash = UserService.calculateMD5(this.tbOldPassword.Text.Trim());
            String NewPasswordHash = UserService.calculateMD5(this.tbPassword.Text.Trim());
            if(OldPasswordHash != this.user.passwordHash) {
                Dialog.showWarning("Password lama salah!");
                continueChange = false;
            }
            if (this.tbPassword.Text != this.tbPassword2.Text || this.tbPassword.Text.Trim() == "") {
                Dialog.showWarning("Password baru dan Password baru (lagi) harus diisi dan harus sama!");
                continueChange = false;
            }
            if (OldPasswordHash == NewPasswordHash) {
                Dialog.showWarning("Password lama dan password baru Anda sama!");
                continueChange = false;
            }

            if (continueChange)
            {
                try
                {
                    if (UserService.changePassword(this.user.username, OldPasswordHash, NewPasswordHash))
                        this.DialogResult = DialogResult.OK;
                    else Dialog.showError("Penggantian password gagal!");
                }
                catch (Exception ex)
                {
                    Dialog.showError(ex.Message);
                }
            }
        }
    }
}
