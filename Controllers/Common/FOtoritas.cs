using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using WIS3.Interfaces;
using Base.Common;

namespace Controllers.Common
{
    public partial class FOtoritas : Form
    {
        private String kode {
            get { return this.tbKode.Text.Trim(); }
            set { this.tbKode.Text = value.Trim(); }
        }
        private String keterangan {
            get { return this.tbKeterangan.Text.Trim(); }
            set { this.tbKeterangan.Text = value.Trim(); }
        }

        private IMainInterface iMainForm;
        public String kodeLama;
        public Otoritas otoritas { get; private set; }

        public FOtoritas(IMainInterface iMainForm) : this()
        {
            this.iMainForm = iMainForm;
        }

        public FOtoritas(IMainInterface iMainForm, Otoritas o) : this(iMainForm)
        {
            this.otoritas = otoritas;
            this.kode = o.controlid;
            this.keterangan = o.keterangan;
            this.kodeLama = o.controlid;
        }

        public FOtoritas()
        {
            this.InitializeComponent();
        }

        private void btOK_Click(object sender, EventArgs e)
        {
            Otoritas o = new Otoritas(this.tbKode.Text, this.tbKeterangan.Text);
            this.otoritas = o;
            this.DialogResult = DialogResult.OK;
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            this.otoritas = null;
            this.DialogResult = DialogResult.Cancel;
        }

        internal void setOtoritas(Otoritas otoritas)
        {
            this.kode = otoritas.controlid;
            this.keterangan = otoritas.keterangan;
        }
    }
}