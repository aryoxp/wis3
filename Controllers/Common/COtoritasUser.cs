using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Equin.ApplicationFramework;
using WIS3.Interfaces;
using Base;
using Controllers;
using Helpers;
using Base.Common;
using Service;
using Service.Common;

namespace Controllers.Common
{
    public partial class COtoritasUser : Controller
    {

        public COtoritasUser()
            : base()
        {
            InitializeComponent();
        }

        public COtoritasUser(IMainInterface mainInterface)
            : base(mainInterface)
        {
            InitializeComponent();
            this.mainInterface = mainInterface;
            if (!this.listUserWorker.IsBusy)
                this.listUserWorker.RunWorkerAsync();
            this.mainInterface.setStatus("Loading users...");
        }

        private void btOtoritas_Click(object sender, EventArgs e)
        {
            this.mainInterface.loadController("Controllers.Common.COtoritas");
        }

        private BindingListView<User> blvUsers;
        private User selectedUser;

        private void btRefresh_Click(object sender, EventArgs e)
        {
            if (!this.listUserWorker.IsBusy && this.selectedUser != null)
            {
                this.listUserWorker.RunWorkerAsync();
                this.mainInterface.setStatus("Loading authorities...");
            }
        }

        private void listUserWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            String error = null;
            List<User> users = UserService.getUserList(ref error);
            DataTable dtOtoritas = OtoritasService.getList(ref error);
            e.Result = new Object[] { users, dtOtoritas, error };
        }

        private void listUserWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Object[] result = (Object[])e.Result;
            List<User> users = (List<User>)result[0];
            DataTable dtOtoritas = (DataTable)result[1];
            String error = (String)result[2];
            this.dgUsers.DataSource = null;
            this.dgUsers.Columns.Clear();
            if (error == null)
            {
                this.blvUsers = new BindingListView<User>(users);
                this.dgUsers.DataSource = this.blvUsers;
                this.dgUsers.Columns[1].Visible = false;
                this.dgUsers.Columns[2].Visible = false;
                this.dgUsers.Columns[3].Visible = false;
                this.dgUsers.Columns[0].HeaderText = "Username";
                this.dgUsers.Columns[0].Width = 100;
                this.dgUsers.Columns[1].HeaderText = "Nama";
                if (this.dgUsers.Columns.Count == 4)
                {
                    DataGridViewImageColumn iconColumn = new DataGridViewImageColumn();
                    iconColumn.HeaderText = "Enabled";
                    this.dgUsers.Columns.Add(iconColumn);
                    this.dgUsers.Columns[4].Width = 70;
                }
                foreach (DataGridViewRow row in this.dgUsers.Rows)
                {
                    if (row.Cells[3].Value.ToString().Equals("True", StringComparison.CurrentCultureIgnoreCase))
                        row.Cells[4].Value = (Image) Controllers.Properties.Resources.bullet_green;
                    else row.Cells[4].Value = (Image) Controllers.Properties.Resources.bullet_red;
                }

                this.dgOtoritas.Columns.Clear();
                this.dgOtoritas.DataSource = dtOtoritas;
                if (dtOtoritas != null)
                {
                    this.dgOtoritas.Columns[0].HeaderText = "Control Name";
                    this.dgOtoritas.Columns[1].HeaderText = "Information";
                }
            }
            else Dialog.showError(error);
            this.mainInterface.setStatusReady();
        }

        private void ListWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            String error = null;
            String username = (String)e.Argument;
            List<String> otoritas = OtoritasService.getOtoritasList(username, ref error);
            e.Result = new Object[] { otoritas, error };
        }

        private void ListWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            
            try
            {
                Object[] result = (Object[])e.Result;
                List<String> otoritas = (List<String>)result[0];
                String error = (String)result[1];
                if (dgOtoritas.Columns.Count == 2)
                {
                    DataGridViewImageColumn enabledColumn = new DataGridViewImageColumn();
                    enabledColumn.HeaderText = "Enabled";
                    this.dgOtoritas.Columns.Add(enabledColumn);
                    this.dgOtoritas.Columns.Add("enabled", "Enabled");
                    this.dgOtoritas.Columns[3].Visible = false;
                    this.dgOtoritas.Columns[2].Width = 70;
                }
                foreach (DataGridViewRow dr in this.dgOtoritas.Rows)
                {
                    String kode = dr.Cells[0].Value.ToString();
                    Boolean allowed = Otoritas.check(kode, otoritas);
                    if (allowed)
                    {
                        dr.Cells[2].Value = Controllers.Properties.Resources.bullet_green;
                        dr.Cells[3].Value = "OK";
                    }
                    else
                    {
                        dr.Cells[2].Value = Controllers.Properties.Resources.bullet_red;
                        dr.Cells[3].Value = "-";
                    }
                }
            }
            catch
            {
                this.dgOtoritas.DataSource = null;
            }
            this.mainInterface.setStatusReady();
            this.dgUsers.Enabled = true;
            
        }

        private void dgUsers_SelectionChanged(object sender, EventArgs e)
        {
            if (this.dgUsers.SelectedRows.Count == 0) return;
            ObjectView<User> userView = this.blvUsers[this.dgUsers.SelectedRows[0].Index];
            User u = userView.Object;
            this.selectedUser = u;
            if (!this.ListWorker.IsBusy)
            {
                this.ListWorker.RunWorkerAsync(u.username);
                this.dgUsers.Enabled = false;
                this.mainInterface.setStatus("Loading authorities...");
            }
        }

        private void dgOtoritas_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (this.selectedUser != null && this.dgOtoritas.SelectedRows.Count > 0)
            {
                //DataRow dr = ((DataTable)this.dgOtoritas.DataSource).Rows[this.dgOtoritas.SelectedRows[0].Index];
                DataGridViewRow dr = this.dgOtoritas.Rows[this.dgOtoritas.SelectedRows[0].Index];
                String username = this.selectedUser.username;
                String controlId = dr.Cells[0].Value.ToString();
                String status = dr.Cells[3].Value.ToString();
                Boolean s = status.Equals("-");
                if (!this.SaveWorker.IsBusy)
                {
                    this.SaveWorker.RunWorkerAsync(new Object[] { username, controlId, s });
                    this.mainInterface.setStatus("Saving authorities...");
                }
            }
        }

        private void SaveWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            Object[] args = (Object[])e.Argument;
            Boolean success = false;
            String username = (String)args[0];
            String controlId = (String)args[1];
            String error = "";
            Boolean status = (Boolean)args[2];
            success = OtoritasService.authorize(username, controlId, status, ref error);
            e.Result = new Object[] { success, error };
        }

        private void SaveWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Object[] result = (Object[])e.Result;
            Boolean success = (Boolean)result[0];
            String error = (String)result[1];
            if (success)
            {
                if (!this.ListWorker.IsBusy)
                    this.ListWorker.RunWorkerAsync(this.selectedUser.username);
                this.mainInterface.setStatus("Loading authorities...");
            }
            else
            {
                Dialog.showError(error);
            }
            this.mainInterface.setStatusReady();
        }

        private void bwOtoritasUser_Click(object sender, EventArgs e)
        {
            if (!this.ListWorker.IsBusy)
            {
                if (this.selectedUser != null)
                {
                    this.ListWorker.RunWorkerAsync(this.selectedUser.username);
                    this.mainInterface.setStatus("Loading authorities...");
                }
                else Dialog.showWarning("Please, select a user on the left pane.");
            }
        }

    }
}