using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Equin.ApplicationFramework;
using Controllers;
using Base;
using WIS3.Interfaces;
using Service;
using Helpers;
using Controllers.Common;

namespace Controllers.Common
{
    public partial class CUserManagement : Controller
    {

        private BindingListView<User> blvListUser;

        public CUserManagement() : base()
        {
            InitializeComponent();
        }


        public CUserManagement(IMainInterface mainInterface)
            : this()
        {
            this.mainInterface = mainInterface;
            this.user = user;

            if (!this.ListWorker.IsBusy)
                this.ListWorker.RunWorkerAsync();
            this.mainInterface.setStatus("Loading authorities...");
        }

        private void ListWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            String error = "";
            List<User> users = UserService.getUserList(ref error);
            e.Result = new Object[] { users, error };
        }

        private void ListWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {

                Object[] result = (Object[])e.Result;
                List<User> dtUser = (List<User>)result[0];
                this.blvListUser = new BindingListView<User>(dtUser);
                String error = (String) result[1];
                this.dgUser.Columns.Clear();
                this.dgUser.DataSource = this.blvListUser;

                if (this.dgUser.Columns.Count == 4)
                {
                    DataGridViewImageColumn iconColumn = new DataGridViewImageColumn();
                    iconColumn.HeaderText = "Status";
                    this.dgUser.Columns.Add(iconColumn);
                    this.dgUser.Columns[3].Visible = false;
                    this.dgUser.Columns[4].Width = 70;
                    this.dgUser.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                }

                foreach(DataGridViewRow dr in this.dgUser.Rows) {
                    if (dr.Cells[3].Value.ToString().Equals("True", StringComparison.CurrentCultureIgnoreCase))
                        dr.Cells[4].Value = Controllers.Properties.Resources.bullet_green;
                    else
                        dr.Cells[4].Value = Controllers.Properties.Resources.bullet_red;
                }
            }
            catch
            {
                this.dgUser.DataSource = null;
            }
            this.mainInterface.setStatusReady();
        }

        private void btBaru_Click(object sender, EventArgs e)
        {
            FUser f = new FUser();
            DialogResult dr = f.ShowDialog();
            if (dr == DialogResult.OK)
            {
                User u = f.newUser;
                if (u != null && !this.SaveWorker.IsBusy)
                    this.SaveWorker.RunWorkerAsync(u);
            }
        }

        private void SaveWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            User u = (User)e.Argument;
            String error = "";
            Boolean success = UserService.create(u, ref error);
            e.Result = new object[] { u, success, error };
        }

        private void SaveWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Object[] res = (Object[])e.Result;
            User u = (User)res[0];
            Boolean success = (Boolean)res[1];
            String error = (String)res[2];
            if (success)
            {
                Dialog.showSuccess("User " + u.nama + " created.");
                if (!this.ListWorker.IsBusy)
                {
                    this.ListWorker.RunWorkerAsync();
                    this.mainInterface.setStatus("Listing users...");
                }
            }
            else Dialog.showError(error);
        }

        private void btEdit_Click(object sender, EventArgs e)
        {
            if (this.dgUser.SelectedRows.Count == 0) return; 
            ObjectView<User> uView = this.blvListUser[this.dgUser.SelectedRows[0].Index];
            User u = uView.Object;
            String oldusername = u.username;
            FUser f = new FUser(u);
            DialogResult dr = f.ShowDialog();
            if (dr == DialogResult.OK && !this.UpdateWorker.IsBusy)
            {
                u = f.newUser;
                Boolean updatePassword = true;
                if (String.IsNullOrEmpty(f.getPasswordText()))
                    updatePassword = false;
                this.UpdateWorker.RunWorkerAsync(new Object[] { u, oldusername, updatePassword });
            }

        }

        private void UpdateWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            Object[] args = (Object[])e.Argument;
            User u = (User)args[0];
            String oldusername = (String)args[1];
            Boolean updatePassword = (Boolean)args[2];
            String error = "";
            Boolean success = UserService.update(u, ref error, oldusername, updatePassword);
            e.Result = new Object[] { success, error };
        }

        private void UpdateWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Object[] res = (Object[])e.Result;
            Boolean success = (Boolean)res[0];
            String error = (String)res[1];

            if (success)
            {
                
                if (!this.ListWorker.IsBusy)
                {
                    this.ListWorker.RunWorkerAsync();
                    this.mainInterface.setStatus("Loading users...");
                    
                }
                Dialog.showSuccess("User information updated.");
            } 
            else Dialog.showError(error);

        }

        private void btDelete_Click(object sender, EventArgs e)
        {
            if (this.dgUser.SelectedRows.Count > 0)
            {
                ObjectView<User> userView = this.blvListUser[this.dgUser.SelectedRows[0].Index];
                User u = userView.Object;
                
                if (Confirm.confirm("Hapus user " + u.username + "?"))
                {
                    if (!this.DeleteWorker.IsBusy)
                    {
                        this.DeleteWorker.RunWorkerAsync(u);
                        this.mainInterface.setStatus("Deleting user...");
                    }
                }
            }
        }

        private void DeleteWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            User u = (User)e.Argument;
            String error = "";
            Boolean success = UserService.delete(u, ref error);
            e.Result = new Object[] { success, error, u };
        }

        private void DeleteWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Object[] res = (Object[])e.Result;
            Boolean success = (Boolean)res[0];
            String error = (String)res[1];
            User u = (User)res[2];
            this.mainInterface.setStatusReady();
            if (success)
            {
                if (!this.ListWorker.IsBusy)
                {
                    this.ListWorker.RunWorkerAsync();
                    this.mainInterface.setStatus("Loading users...");
                }
                Dialog.showSuccess("User deleted.");
            }
            else Dialog.showError(error);
        }

        private void btRefresh_Click(object sender, EventArgs e)
        {
            if (!this.ListWorker.IsBusy)
            {
                this.ListWorker.RunWorkerAsync();
                this.mainInterface.setStatus("Loading users...");
            }
        } 
    }
}