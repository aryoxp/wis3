﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WIS3.Interfaces;
using Helpers;
using Service;
using Base;
using Service.Common;
using Base.Common;

namespace Controllers.Common
{
    public partial class CLogin : Controller
    {
        public CLogin() : base()
        {
            InitializeComponent();
        }

        public CLogin(IMainInterface mainInterface)
            : base(mainInterface)
        {
            InitializeComponent();
            init();
        }

        private void btLogin_Click(object sender, EventArgs e)
        {
            doLogin();
        }

        private void doLogin()
        {
            String username = this.tbUsername.Text.Trim();
            String password = this.tbPassword.Text.Trim();

            if (username.Equals("Username") || password.Equals("Password"))
            {
                Dialog.showWarning("Username and password must not empty.");
                return;
            }

            if (!this.bwLogin.IsBusy)
            {
                this.bwLogin.RunWorkerAsync(new Object[] { username, password });
                this.mainInterface.setStatus("Logging in...");
            }
        }

        private void bwLogin_DoWork(object sender, DoWorkEventArgs e)
        {
            Object[] args = (Object[]) e.Argument;
            String username = (String)args[0];
            String password = (String)args[1];
            String error = null;
            User user = null;
            Boolean success = UserService.authenticate(username, UserService.calculateMD5(password), ref error);
            List<String> authorizingControls = OtoritasService.getAllControlIds(ref error);
            if (success)
            {
                user = UserService.getUser(username);
                user.setAuthority(UserService.getOtoritasUser(user.username, ref error));
            }
            e.Result = new Object[] { success, user, authorizingControls, error };
        }

        private void bwLogin_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.mainInterface.setStatusReady();
            Object[] result = (Object[])e.Result;
            Boolean success = (Boolean)result[0];
            User user = (User)result[1];
            List<String> controls = (List<String>)result[2];
            String error = (String)result[3];
            if (error != null)
            {
                Dialog.showError(error);
                return;
            }
            if (user != null)
            {
                this.mainInterface.setUser(user);
                this.mainInterface.setAuthorizingControls(controls);
                this.mainInterface.loadController("Controllers.Akademik.CDashboard");
            } else
                Dialog.showError("Unable to log you in, incorrect username and/or password.");
            this.mainInterface.setConnection(WIS3.Base.ConnectionStatus.Connected);
        }

        private void tbPassword_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                this.doLogin();
        }

    }
}
