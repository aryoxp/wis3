﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WIS3.Interfaces;
using Base;

namespace Controllers
{
    public partial class Controller : UserControl
    {

        protected IMainInterface mainInterface;
        protected User user;

        public Controller()
        {
            InitializeComponent();
        }

        public Controller(IMainInterface mainInterface) : this()
        {
            this.user = mainInterface.getUser();
            this.mainInterface = mainInterface;
        }
    }
}
