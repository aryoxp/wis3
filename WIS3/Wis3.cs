﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WIS3.Interfaces;
using Base;
using WIS3.Base;
using Helpers;
using System.Diagnostics;
using Controllers.Common;

namespace WIS3
{
    public partial class Wis3 : Form, IMainInterface   
    {

        private void menuExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void menuReportUser_Click(object sender, EventArgs e)
        {
            this.loadController("Controllers.Akademik.UserController");
        }

        private void menuLogin_Click(object sender, EventArgs e)
        {
            this.loadController("Controllers.Common.CLogin");
        }

        private void menuAuthorityManagement_Click(object sender, EventArgs e)
        {
            this.loadController("Controllers.Common.COtoritas");
        }

        private void menuUserAuthority_Click(object sender, EventArgs e)
        {
            this.loadController("Controllers.Common.COtoritasUser");
        }

        private void menuSettingsUserManagement_Click(object sender, EventArgs e)
        {
            this.loadController("Controllers.Common.CUserManagement");
        }

    }

}
