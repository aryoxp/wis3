﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Helpers;

namespace WIS3
{
    public partial class ConnectionSetup : Form
    {
        public String error;

        public ConnectionSetup()
        {
            InitializeComponent();
            try
            {
                DatabaseConfig config = Config.readDatabaseConfig();
                this.tbHost.Text = config.host;
                this.tbPort.Text = config.port;
                this.tbUsername.Text = config.username;
                this.tbPassword.Text = config.password;
                this.tbDatabase.Text = config.dbname;
            }
            catch (Exception exception) {
                this.error = exception.Message;
            }
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btOK_Click(object sender, EventArgs e)
        {
            if (Confirm.confirm("Save connection data?"))
            {
                DatabaseConfig config = new DatabaseConfig();
                config.host = this.tbHost.Text.Trim();
                config.port = this.tbPort.Text.Trim();
                config.username = this.tbUsername.Text.Trim();
                config.password = this.tbPassword.Text.Trim();
                config.dbname = this.tbDatabase.Text.Trim();

                if (Config.saveDatabaseConfig(config))
                    this.DialogResult = DialogResult.OK;
                else Dialog.showError("Unable to save server configuration data.");
            }
        }
    }
}
