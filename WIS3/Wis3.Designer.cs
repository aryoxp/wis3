﻿namespace WIS3
{
    partial class Wis3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.StatusStrip statusBar;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Wis3));
            this.mainProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.mainStatusText = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.menuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.menuLogout = new System.Windows.Forms.ToolStripMenuItem();
            this.menuLogin = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.menuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.reportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuReportUser = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSettingsServerConnection = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSettingsUserManagement = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSettingsAuthority = new System.Windows.Forms.ToolStripMenuItem();
            this.menuAuthorityManagement = new System.Windows.Forms.ToolStripMenuItem();
            this.menuUserAuthority = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.menuSettingsUpdate = new System.Windows.Forms.ToolStripMenuItem();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.bwConnectionTest = new System.ComponentModel.BackgroundWorker();
            this.menuSettingsChangePassword = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.mainHostText = new System.Windows.Forms.ToolStripStatusLabel();
            statusBar = new System.Windows.Forms.StatusStrip();
            statusBar.SuspendLayout();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusBar
            // 
            statusBar.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            statusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mainHostText,
            this.mainProgressBar,
            this.mainStatusText});
            statusBar.Location = new System.Drawing.Point(0, 329);
            statusBar.Name = "statusBar";
            statusBar.Padding = new System.Windows.Forms.Padding(1, 0, 16, 0);
            statusBar.Size = new System.Drawing.Size(596, 30);
            statusBar.TabIndex = 0;
            statusBar.Text = "statusStrip1";
            // 
            // mainProgressBar
            // 
            this.mainProgressBar.Margin = new System.Windows.Forms.Padding(5, 5, 2, 5);
            this.mainProgressBar.Name = "mainProgressBar";
            this.mainProgressBar.Size = new System.Drawing.Size(117, 20);
            this.mainProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            // 
            // mainStatusText
            // 
            this.mainStatusText.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.mainStatusText.Name = "mainStatusText";
            this.mainStatusText.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.mainStatusText.Size = new System.Drawing.Size(48, 25);
            this.mainStatusText.Text = "Ready";
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = System.Drawing.Color.White;
            this.menuStrip.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFile,
            this.reportToolStripMenuItem,
            this.menuSettings});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            this.menuStrip.Size = new System.Drawing.Size(596, 24);
            this.menuStrip.TabIndex = 1;
            this.menuStrip.Text = "menuStrip1";
            // 
            // menuFile
            // 
            this.menuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuLogout,
            this.menuLogin,
            this.toolStripSeparator1,
            this.menuExit});
            this.menuFile.Name = "menuFile";
            this.menuFile.Size = new System.Drawing.Size(40, 20);
            this.menuFile.Text = "File";
            // 
            // menuLogout
            // 
            this.menuLogout.Name = "menuLogout";
            this.menuLogout.Size = new System.Drawing.Size(114, 22);
            this.menuLogout.Text = "Logout";
            this.menuLogout.Click += new System.EventHandler(this.menuLogout_Click);
            // 
            // menuLogin
            // 
            this.menuLogin.Name = "menuLogin";
            this.menuLogin.Size = new System.Drawing.Size(114, 22);
            this.menuLogin.Text = "Login";
            this.menuLogin.Click += new System.EventHandler(this.menuLogin_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(111, 6);
            // 
            // menuExit
            // 
            this.menuExit.Name = "menuExit";
            this.menuExit.Size = new System.Drawing.Size(114, 22);
            this.menuExit.Text = "Exit";
            this.menuExit.Click += new System.EventHandler(this.menuExit_Click);
            // 
            // reportToolStripMenuItem
            // 
            this.reportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuReportUser});
            this.reportToolStripMenuItem.Name = "reportToolStripMenuItem";
            this.reportToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.reportToolStripMenuItem.Text = "Report";
            // 
            // menuReportUser
            // 
            this.menuReportUser.Name = "menuReportUser";
            this.menuReportUser.Size = new System.Drawing.Size(102, 22);
            this.menuReportUser.Text = "User";
            this.menuReportUser.Click += new System.EventHandler(this.menuReportUser_Click);
            // 
            // menuSettings
            // 
            this.menuSettings.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuSettingsChangePassword,
            this.toolStripSeparator4,
            this.menuSettingsUserManagement,
            this.menuSettingsAuthority,
            this.toolStripSeparator2,
            this.menuSettingsServerConnection,
            this.menuSettingsUpdate});
            this.menuSettings.Name = "menuSettings";
            this.menuSettings.Size = new System.Drawing.Size(66, 20);
            this.menuSettings.Text = "Settings";
            // 
            // menuSettingsServerConnection
            // 
            this.menuSettingsServerConnection.Name = "menuSettingsServerConnection";
            this.menuSettingsServerConnection.Size = new System.Drawing.Size(181, 22);
            this.menuSettingsServerConnection.Text = "Server Connection";
            this.menuSettingsServerConnection.Click += new System.EventHandler(this.menuSettingsServerConnection_Click);
            // 
            // menuSettingsUserManagement
            // 
            this.menuSettingsUserManagement.Name = "menuSettingsUserManagement";
            this.menuSettingsUserManagement.Size = new System.Drawing.Size(181, 22);
            this.menuSettingsUserManagement.Text = "User Management";
            this.menuSettingsUserManagement.Click += new System.EventHandler(this.menuSettingsUserManagement_Click);
            // 
            // menuSettingsAuthority
            // 
            this.menuSettingsAuthority.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuAuthorityManagement,
            this.menuUserAuthority});
            this.menuSettingsAuthority.Name = "menuSettingsAuthority";
            this.menuSettingsAuthority.Size = new System.Drawing.Size(181, 22);
            this.menuSettingsAuthority.Text = "Authority";
            // 
            // menuAuthorityManagement
            // 
            this.menuAuthorityManagement.Name = "menuAuthorityManagement";
            this.menuAuthorityManagement.Size = new System.Drawing.Size(205, 22);
            this.menuAuthorityManagement.Text = "Authority Management";
            this.menuAuthorityManagement.Click += new System.EventHandler(this.menuAuthorityManagement_Click);
            // 
            // menuUserAuthority
            // 
            this.menuUserAuthority.Name = "menuUserAuthority";
            this.menuUserAuthority.Size = new System.Drawing.Size(205, 22);
            this.menuUserAuthority.Text = "User Authority";
            this.menuUserAuthority.Click += new System.EventHandler(this.menuUserAuthority_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(178, 6);
            // 
            // menuSettingsUpdate
            // 
            this.menuSettingsUpdate.Name = "menuSettingsUpdate";
            this.menuSettingsUpdate.Size = new System.Drawing.Size(181, 22);
            this.menuSettingsUpdate.Text = "Update...";
            this.menuSettingsUpdate.Click += new System.EventHandler(this.menuSettingsUpdate_Click);
            // 
            // mainPanel
            // 
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPanel.Location = new System.Drawing.Point(0, 24);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(596, 305);
            this.mainPanel.TabIndex = 2;
            this.mainPanel.ControlAdded += new System.Windows.Forms.ControlEventHandler(this.mainPanel_ControlAdded);
            // 
            // bwConnectionTest
            // 
            this.bwConnectionTest.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwConnectionTest_DoWork);
            this.bwConnectionTest.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwConnectionTest_RunWorkerCompleted);
            // 
            // menuSettingsChangePassword
            // 
            this.menuSettingsChangePassword.Name = "menuSettingsChangePassword";
            this.menuSettingsChangePassword.Size = new System.Drawing.Size(181, 22);
            this.menuSettingsChangePassword.Text = "Change Password";
            this.menuSettingsChangePassword.Click += new System.EventHandler(this.menuSettingsChangePassword_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(178, 6);
            // 
            // mainHostText
            // 
            this.mainHostText.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.mainHostText.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.mainHostText.Image = global::WIS3.Properties.Resources.indicatorred;
            this.mainHostText.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.mainHostText.Name = "mainHostText";
            this.mainHostText.Size = new System.Drawing.Size(115, 21);
            this.mainHostText.Text = "127.0.0.1";
            // 
            // Wis3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(596, 359);
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(statusBar);
            this.Controls.Add(this.menuStrip);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Wis3";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Wira Husada Nusantara";
            statusBar.ResumeLayout(false);
            statusBar.PerformLayout();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripProgressBar mainProgressBar;
        private System.Windows.Forms.ToolStripStatusLabel mainStatusText;
        private System.Windows.Forms.ToolStripStatusLabel mainHostText;
        private System.Windows.Forms.ToolStripMenuItem menuFile;
        private System.Windows.Forms.ToolStripMenuItem menuExit;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.ToolStripMenuItem reportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuReportUser;
        private System.ComponentModel.BackgroundWorker bwConnectionTest;
        private System.Windows.Forms.ToolStripMenuItem menuSettings;
        private System.Windows.Forms.ToolStripMenuItem menuSettingsServerConnection;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem menuSettingsAuthority;
        private System.Windows.Forms.ToolStripMenuItem menuAuthorityManagement;
        private System.Windows.Forms.ToolStripMenuItem menuUserAuthority;
        private System.Windows.Forms.ToolStripMenuItem menuLogout;
        private System.Windows.Forms.ToolStripMenuItem menuLogin;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem menuSettingsUserManagement;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem menuSettingsUpdate;
        private System.Windows.Forms.ToolStripMenuItem menuSettingsChangePassword;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
    }
}

