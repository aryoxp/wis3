﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIS3.Interfaces;
using System.Windows.Forms;
using WIS3.Base;
using Base;
using System.ComponentModel;
using Helpers;
using System.Drawing;
using System.Diagnostics;
using Controllers.Common;

namespace WIS3
{
    public partial class Wis3 : Form, IMainInterface
    {

        private User user;
        private List<String> alwaysEnabledMenu = new List<String>() { 
            "menuFile", "menuSettings", "menuLogin", "menuLogout", "menuExit", "menuSettingsServerConnection", "menuSettingsUpdate"
        };
        private List<String> alwaysEnabledMenuAfterLogin = new List<String>()
        {
            "menuSettingsChangePassword"
        };
        private List<String> authorizingControls;

        public Wis3()
        {
            InitializeComponent();
            InitializeUI();
            this.Size = new Size(1000, 600);
            if (this.user == null)
            {
                this.loadController("Controllers.Common.CLogin");
                this.menuLogout.Visible = false;
            }
            if (!this.bwConnectionTest.IsBusy)
            {
                this.bwConnectionTest.RunWorkerAsync();
                this.setConnection(ConnectionStatus.Connecting);
                this.setStatus("Connecting to server...");
            }
        }

        public void setStatus(string statusText)
        {
            this.mainStatusText.Text = statusText;
            this.mainProgressBar.Style = ProgressBarStyle.Marquee;
        }
        public void setStatusReady()
        {
            this.mainStatusText.Text = "Ready";
            this.mainProgressBar.Style = ProgressBarStyle.Blocks;
        }
        public void setStatusReady(String status)
        {
            this.mainStatusText.Text = status;
            this.mainProgressBar.Style = ProgressBarStyle.Blocks;
        }
        public void setConnection(ConnectionStatus connectionStatus)
        {
            switch (connectionStatus)
            {
                case ConnectionStatus.NotConnected:
                    this.mainHostText.Image = Properties.Resources.indicatorred;
                    break;
                case ConnectionStatus.Connecting:
                    this.mainHostText.Image = Properties.Resources.indicatoryellow;
                    break;
                case ConnectionStatus.Connected:
                    this.mainHostText.Image = Properties.Resources.indicatorgreen;
                    break;
                default:
                    this.mainHostText.Image = Properties.Resources.indicatorgrey;
                    break;
            }
        }
        public void setHost(string host)
        {
            this.mainHostText.Text = host;
        }
        public void setUser(User user)
        {
            this.user = user;
            this.menuLogout.Visible = true;
            this.menuLogin.Visible = false;
        }
        public User getUser()
        {
            return this.user;
        }

        public void setAuthorizingControls(List<String> controls)
        {
            this.authorizingControls = controls;
        }

        private void bwConnectionTest_DoWork(object sender, DoWorkEventArgs e)
        {
            e.Result = Database.connectionTest();
        }
        private void bwConnectionTest_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if ((Boolean)e.Result)
            {
                DatabaseConfig config = Config.readDatabaseConfig();
                this.setConnection(ConnectionStatus.Connected);
                this.setHost(config.host + ":" + config.port);
                this.setStatusReady();
            }
            else
            {
                this.setConnection(ConnectionStatus.NotConnected);
                this.setStatusReady("Unable to connect to server.");
            }
        }

        // ControllerLoader
        public void loadController(String viewType)
        {
            try
            {
                User user = this.user;
                UserControl control = (UserControl)Activator.CreateInstance(Type.GetType(viewType + ", Controllers"),
                    new Object[] { this });
                control.Dock = DockStyle.Fill;
                this.mainPanel.Controls.Clear();
                this.mainPanel.Controls.Add(control);
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
            }
        }
        private void mainPanel_ControlAdded(object sender, ControlEventArgs e)
        {
            if (mainPanel.Controls.Count > 0)
            {
                foreach (Control c in mainPanel.Controls)
                {
                    c.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Right;
                    c.Height = mainPanel.Height;
                    c.Width = mainPanel.Width;
                    c.Location = new Point(0, 0);
                    if (c.Location.Y + c.Height > mainPanel.Height)
                        mainPanel.AutoScroll = true;
                }
                this.authorizeUI();
            }
        }

        private void InitializeUI()
        {
            try
            {
                DatabaseConfig config = Config.readDatabaseConfig();
                this.mainHostText.Text = config.host + ":" + config.port;
            }
            catch (Exception ex)
            {
                Dialog.showError(ex.Message);
            }
        }

        public void authorizeUI()
        {
            loopControls(this.Controls);
            loopMenuItems(this.menuStrip);
        }

        private void checkAlwaysEnabledMenu(ToolStripMenuItem item)
        {
            foreach (String s in this.alwaysEnabledMenu)
            {
                if (item.Name.Equals(s))
                    item.Enabled = true;
            }
        }

        private void checkAlwaysEnabledMenu(ToolStripItem item)
        {
            foreach (String s in this.alwaysEnabledMenu)
            {
                if(item.Name.Equals(s))
                    item.Enabled = true;
            }
            if (this.user != null)
            {
                foreach (String s in alwaysEnabledMenuAfterLogin)
                {
                    if (item.Name.Equals(s))
                        item.Enabled = true;
                }
            }
        }

        private void loopMenuItems(MenuStrip menuStrip)
        {
            foreach (ToolStripMenuItem i in menuStrip.Items)
            {
                try
                {
                    if (this.checkAuthorizingControl(i.Name))
                    {
                        if (this.user.isAuthorizedFor(i.Name))
                            i.Enabled = true;
                        else i.Enabled = false;
                    }
                }
                catch { i.Enabled = false; }
                this.checkAlwaysEnabledMenu(i);
                this.loopDropMenuItems(i);
            }
        }

        private void loopDropMenuItems(ToolStripMenuItem item)
        {
            foreach (ToolStripItem i in item.DropDownItems)
            {
                try
                {
                    if (this.checkAuthorizingControl(i.Name))
                    {
                        if (this.user.isAuthorizedFor(i.Name))
                            i.Enabled = true;
                        else i.Enabled = false;
                    }
                }
                catch {
                    i.Enabled = false;
                }
                this.checkAlwaysEnabledMenu(i);
                if (i is ToolStripMenuItem)
                    loopDropMenuItems((ToolStripMenuItem)i);
                
            }
        }

        public void loopControls(Control.ControlCollection controls)
        {
            try
            {
                foreach (Control c in controls)
                {
                    if (this.checkAuthorizingControl(c.Name))
                    {
                        if (c is Button || c is CheckBox || c is ComboBox || c is RadioButton || c is GroupBox)
                        {
                            if (this.user == null) continue;
                            if (this.user.isAuthorizedFor(c.Name))
                                c.Enabled = true;
                            else c.Enabled = false;
                        }
                    }
                    if (c.Controls.Count > 0)
                    {
                        loopControls(c.Controls);
                    }
                }
            }
            catch {}
        }

        private bool checkAuthorizingControl(String controlName)
        {
            foreach (String s in this.authorizingControls)
            {
                if (s.Equals(controlName))
                    return true;
            }
            return false;
        }

        private void menuSettingsUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start("Updater.exe");
            }
            catch { Dialog.showError("Unable to start application updater. Please update the application manually and refers to the instructions manual."); }
        }

        private void menuSettingsServerConnection_Click(object sender, EventArgs e)
        {
            ConnectionSetup connectionSetup = new ConnectionSetup();
            if (connectionSetup.ShowDialog() == DialogResult.OK)
            {
                InitializeUI();

                if (!this.bwConnectionTest.IsBusy)
                {
                    this.bwConnectionTest.RunWorkerAsync();
                    this.setConnection(ConnectionStatus.Connecting);
                    this.setStatus("Connecting to server...");
                }

            }
        }

        private void menuLogout_Click(object sender, EventArgs e)
        {
            this.doLogout();
        }

        private void doLogout()
        {
            this.user = null;
            this.authorizeUI();
            this.loadController("Controllers.Common.CLogin");
            this.menuLogout.Visible = false;
            this.menuLogin.Visible = true;
        }

        private void menuSettingsChangePassword_Click(object sender, EventArgs e)
        {
            FPassword f = new FPassword(this.user);
            if (f.ShowDialog() == DialogResult.OK)
            {
                Dialog.showSuccess("Password has been updated successfully. Please re-login.");
                this.doLogout();
            }
        }
    }
}
