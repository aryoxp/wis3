﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Base
{
    public class User
    {
        private List<string> authorizedControlNames;

        public User(string username, string passwordHash, string nama, string status)
        {
            this.username = username;
            this.passwordHash = passwordHash;
            this.status = status;
            this.nama = nama;
        }

        public String username { get; set; }
        public String nama { get; set; }
        public String passwordHash { get; set; }
        public String status { get; set; }

        public User(string username, string passwordHash, string nama, Boolean status)
        {
            this.username = username;
            this.passwordHash = passwordHash;
            if (status) this.status = "1";
            else this.status = "0";
            this.nama = nama;
        }

        public void authorizeControlNames(List<String> controlNames)
        {
            this.authorizedControlNames = controlNames;
        }

        public Boolean isAuthorizedFor(String controlName)
        {
            if(this.authorizedControlNames == null) 
                return false;
            foreach (String control in this.authorizedControlNames)
            {
                if(control.Equals(controlName)) {
                    return true;
                }
            }
            return false;
        }


        public void setAuthority(List<string> list)
        {
            this.authorizedControlNames = list;
        }
    }
}
