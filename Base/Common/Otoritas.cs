﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Base.Common
{
    public class Otoritas
    {
        public String controlid;
        public String keterangan;

        public Otoritas(String controlid, String keterangan)
        {
            this.controlid = controlid;
            this.keterangan = keterangan;
        }

        public static Boolean check(string controlid, List<string> otoritas)
        {
            foreach (String c in otoritas)
            {
                if (controlid.Equals(c))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
