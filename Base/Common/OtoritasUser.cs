﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Base.Common
{
    public class OtoritasUser
    {
        public User user;
        public Otoritas otoritas;


        public OtoritasUser(User user, Otoritas otoritas)
        {
            this.user = user;
            this.otoritas = otoritas;
        }
    }
}
