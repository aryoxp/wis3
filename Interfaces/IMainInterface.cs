﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIS3.Base;
using Base;

namespace WIS3.Interfaces
{
    public interface IMainInterface
    {
        void setStatus(String statusText);
        void setStatusReady();
        void setStatusReady(String status);
        void setConnection(ConnectionStatus connectionStatus);
        void setHost(String host);
        void loadController(String controller);
        void setUser(User user);
        User getUser();
        void setAuthorizingControls(List<String> controls);
    }
}
